package Controller;
import Exceptions.FileException;
import Exceptions.InterpretorException;
import Model.ADT.*;
import Model.PrgState;
import Model.Statement.*;
import Model.Expression.*;
import Repository.PrgRepository;
import Repository.IRepository;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class Controller {
    IRepository repo;
    ExecutorService executor;

    public Controller(IRepository r)
    {
        this.repo=r;
    }

   /* public void executeOneStep()
    {
        PrgState prg= repo.getCurrentPrgState();
        IStack<Statement> exestack=prg.getExeStack();
        if(exestack.isEmpty())
            return;
        Statement instruction = exestack.pop();
        instruction.execute(prg);
        prg.getHeap().setContent(conservativeGarbageCollector(prg.getSymbolTable().getValues(),prg.getHeap()));
        System.out.println(prg);
    }*/
    public List<PrgState> removeCompletedPrg(List<PrgState> prgList)
    {
        return prgList.stream().filter(p->p.isNotCompleted()).collect(Collectors.toList());
    }

    /*public void executeAll()
    {
        PrgState ps = repo.getCurrentPrgState();
        IStack<Statement> exeStack = ps.getExeStack();
        try {
            while (!exeStack.isEmpty()) {
                this.executeOneStep();


                repo.logPrgStateExec();
            }
        }
        catch(InterpretorException e)
        {
            System.out.println(e.toString());
        }
        finally {
            ps.getFileTable().getAllValues().stream().forEach(
                    e->{
                        try{
                            e.getHeader().close();
                        }
                        catch (IOException ex)
                        {
                            throw new FileException(ex.toString());
                        }
                    });
        }

    }*/

    public void oneStepForAl(List<PrgState> l)
    {
        List<Callable<PrgState>> lc = l.stream()
                                       .map((PrgState p)->(Callable<PrgState>)(()->{return p.executeOneStep();}))
                                       .collect(Collectors.toList());
        List<PrgState> lp = null;
        try
        {
            lp=executor.invokeAll(lc).stream()
                                     .map(future->
                                             {
                                                 try{
                                                     return future.get();
                                                 }catch (InterruptedException e)
                                                 {
                                                     throw new InterpretorException(e.getMessage());
                                                 }
                                                 catch (ExecutionException e)
                                                 {
                                                     throw  new InterpretorException(e.getMessage());
                                                 }
                                             }
                                     ).filter(p->p!=null).collect(Collectors.toList());
        }
        catch (InterruptedException e)
        {
            throw  new InterpretorException(e.getMessage());
        }
        l.forEach(p->repo.logPrgStateExec(p));
        l.addAll(lp);
        repo.setPrgStates(l);
    }

    public void allStep()
    {
        this.executor= Executors.newFixedThreadPool(3);
        List<PrgState> prg = removeCompletedPrg(repo.getPrgStates());
        while(prg.size()!=0)
        {
            oneStepForAl(prg);
            prg=removeCompletedPrg(repo.getPrgStates());
        }

        executor.shutdown();
        repo.setPrgStates(prg);

    }
    private Map<Integer,Integer> conservativeGarbageCollector(List<Integer> symTableValues, IHeap<Integer,Integer> heap)
    {
        return heap.entrySet().stream()
                .filter(e->symTableValues.contains(e.getKey()))
                .collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));
    }

}
