package Model.Statement;

import Model.ADT.*;
import Model.FileData;
import Model.PrgState;

public class ForkStatement implements  Statement {

    private Statement stmt;

    public ForkStatement(Statement s)
    {
        this.stmt =s;
    }

    @Override
    public PrgState execute(PrgState p) {
        IStack<Statement> stack = p.getExeStack();
        IDictionary<String,Integer> dict = p.getSymbolTable().copy();
        IHeap<Integer,Integer> heap = p.getHeap();
        IFileTable<Integer, FileData> filetable = p.getFileTable();
        IList<Integer> output = p.getMessages();
        Statement root = p.getRootp();

        PrgState prg = new PrgState(stack,dict,output,root,filetable,heap);
        return prg;
    }

    @Override
    public String toString() {
        return "fork("+this.stmt+");\n";
    }
}

